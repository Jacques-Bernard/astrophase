# AstroPhase

- [Fr] Module d'affichage de la phase d'un astre vue depuis la Terre à  une date donnée.
- [En] Module for displaying the phase of a star seen from the Earth at a given date.

Copyright (c) 2012, The Practice Astronom Guide Book Project